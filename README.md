[TOC]

# Synopsis

Cohortable Website

# Getting Started

## Installation

    # Globally install the gulp and bower binaries
    npm -g gulp bower
    # Install the server-side libraries in to ./node_modules
    npm install
    # Install the client-side libraries in to ./client/vendor
    bower install

## Directory structure

All client-side code resides in the client directory

    client/
    ├── app # Stores the actual application
    │   ├── common # Contains common elements used throughout the application
    │   ├── header # Header component
    │   ├── home # Homepage component
    │   ├── login
    │   ├── partials # Contains partial html templates
    │   ├── register
    │   └── security # Contains service that handles authentiation and user information
    ├── css # Contains all CSS/SCSS files
    ├── images # Image/icon assets
    └── vendor # Contains 3rd party libraries

All server-side code resides in the server directory

    server # Contains the base server files
    ├── api # This is where the magic happens
    ├── config # Holds the configuration of the application and setting up bookshelf
    ├── db # Holds information regarding our database
    │   ├── migrations # DB Migration files
    │   └── seeds # Seeds basic information and dummy user data for testing
    ├── errors # Custom error classes we can use throughout the application
    ├── models # Holds the definition for the bookshelf models
    ├── routes # Defines the various routes for the application
    └── server.js # Main entry point for the application


All test code resides in the test directory

    test
    ├── client # Contains client tests
    │   └── unit # Basic unit tests
    │   └── e2e # End-to-end tests
    ├── coverage # Code coverage reports
    └── server # Server-side unit/route tests

## Gulp

There are a number of gulp tasks that are useful for development

    # Runs the basic development tasks which will autocompile CSS and auto-refresh your browser window. This should open up a new window to localhost:3100
    gulp

    # This will run both the client-side and server-side unit tests one time
    gulp test

    # This will run only the client-side tests
    gulp karma

    # This will run only the server-side tests
    gulp mocha

## Fig

You can spin up an isolated test instance of the application in order to run unit/end-to-end tests

    fig build
    fig up

## Docker

Sometimes there may be a problem running Fig (e.g. a container is left running)

    # List out the images that are still running
    docker ps images
    # Stop an instance
    docker stop mongodb
    # Remote the named instance
    docker rm mongodb

# Development

## Client Side

## Server Side

The server can be started up in to 3 different development environments:
`development`, `test`, `production`.

`development` should be used for day to day development work. It will connect
to a mongdb instance located locally on the box. It also defines it's own
special key for the json web token.

`test` is set up to run inside of a docker instance. It expects to contact the
mongodb database at a particular endpoint.

`production` has yet to be determined how production will be set up.

Currently the server codebase is split in to 3 distinct areas that each serve their own purpose:

* Configuration - This controls the configuration and start-up of the web
  server
* Route Definition - This defines the API endpoints, forwards the request on to
  the appropriate handler, and then deal with the responses/errors from
  handler.
* Business Logic - Recevies requests, does work, and returns an async response.

### Database

User are using `knex` to handle the database table creation and migration. In order to create a new migration,
perform the following

    knex --knexfile server/config/knexfile.js migrate:make MIGRATION_NAME

In order to apply the latest changes

    knex --knexfile server/config/knexfile.js migrate:latest

To create new seed data

    knex --knexfile server/config/knexfile.js server seed:make

And to apply the new seed data

    knex --knexfile server/config/knexfile.js server seed:run

There is a helpful gulp command that will nuke the DB and then re-seed it

    gulp refreshdb

### Debugging

We are making use of the `debug` library for node. What that allows you to do is define the `DEBUG` environment variable to determine what type of debugging information you want to see. Instead of defining levels you define keywords for each area of code.

All our debugging SHOULD be prefixed with `co:` so to see all debugging information for our application, you can do something like

    DEBUG="co:*" gulp

If you are only interested in the authentication and mongodb portion of the code you can do:

    DEBUG="co:auth,co:mongo" gulp

### Configuration

Located in `config` directory it contains configuration information specific
for each of the different environments.

### Route Definition

Route definitions can be thought of as the controller logic that maps and API
endpoint in ExpressJS world to the core business logic. The separation allows
us to the test the route functionality separate from the business logic.

### Business Logic

This is the meat of the application. It will receive work requests from the
route definitions, process the request and return a promise of work. All
libraries should be an async library for consistency and to avoid callback
hell. Using bluebird we can promisfy anything including core node APIs
