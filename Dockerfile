FROM node:0.10.35
MAINTAINER Nathan Smith <nathasm@gmail.com>
ENV PHANTOMJS_VERSION 1.9.7

RUN wget --no-check-certificate -q -O - https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-$PHANTOMJS_VERSION-linux-x86_64.tar.bz2 | tar xjC /opt
RUN ln -s /opt/phantomjs-$PHANTOMJS_VERSION-linux-x86_64/bin/phantomjs /usr/local/bin/phantomjs
RUN npm install -g gulp bower

ADD package.json /src/package.json
ADD bower.json /src/bower.json
ADD .bowerrc /src/.bowerrc

RUN cd /src && npm install
RUN cd /src && bower --config.interactive=false --allow-root install

WORKDIR /src

ADD . /src

CMD gulp mocha
