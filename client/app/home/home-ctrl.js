(function() {
  'use strict';

  angular.module('co.home.controller', [
    'ui.router',
    'co.security'
  ])
  .config(function config($stateProvider) {
    $stateProvider
    .state('app.home', {
      url: '/',
      views: {
        'header@': {
          template: ''
        },
        'content@': {
          controller: 'HomeCtrl',
          controllerAs: 'home',
          templateUrl: '/app/home/home.tpl.html'
        },
        'input@app.home': {
          templateUrl: '/app/home/register.tpl.html'
        },
        'link@app.home': {
          template: '<p>Already a member? <a ui-sref="app.home.login">Login</a>'
        },
        'button@app.home': {
          template: '<p><a href ng-click="home.register()" class="btn btn-primary">Sign Up for Free</a>'
        }

      },
      data: { pageTitle: 'Cohortable' }
    })
    .state('app.home.login', {
      url: 'login',
      views: {
        'input@app.home': {
          templateUrl: '/app/home/login.tpl.html'
        },
        'link@app.home': {
          template: '<p>Need to sign-up? <a ui-sref="app.home">Register</a>'
        },
        'button@app.home': {
          template: '<p><a href ng-click="home.login()" class="btn btn-primary">Login</a>'
        }
      }
    });

  })
  .controller('HomeCtrl', HomeCtrl);

  HomeCtrl.$inject = ['security', '$state'];

  function HomeCtrl(security, $state) {
    var vm = this;
    vm.state = $state;
    vm.security = security;

    vm.register = function register() {
      security.register(vm.username, vm.password, vm.passwordConfirmation, vm.email);
    };

    vm.login = function login() {
      security.login(vm.username, vm.password);
    };
  }

})();
