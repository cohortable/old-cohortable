//Ref. polifyll https://github.com/davidchambers/Base64.js
//this is used to parse the profile contained in the JWT

angular.module('co.security', [
  'co.http'
])
.service('security', function($rootScope, httpService, $window, $location) {

  var service = {
    isAuthenticated: false,
    currentUser: undefined,
    login: login,
    logout: logout,
    register: register,
    url_base64_decode: url_base64_decode
  };
  return service;

  function login(username, password) {
    httpService.login(username, password)
    .then(function (data) {
      $window.sessionStorage.token = data.token;
      service.isAuthenticated = true;

      var encodedProfile = data.token.split('.')[1];
      service.currentUser = JSON.parse(service.url_base64_decode(encodedProfile));
    });
  }

  function logout() {
    httpService.logout()
    .then(function (data) {
      service.isAuthenticated = false;

      //Erase JWT token if the user fails to log in
      delete $window.sessionStorage.token;
      $location.url('/');
    });
  }

  function register(username, password, passwordConfirmation, email) {
    if (service.isAuthenticated) {
      return;
    } else {
      httpService.register(username, password, passwordConfirmation, email)
      .then(function(data) {
        $location.path('/');
      });
    }
  }

  function url_base64_decode(str)
  {
    var output = str.replace('-', '+').replace('_', '/');
    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += '==';
        break;
      case 3:
        output += '=';
        break;
      default:
        throw 'Illegal base64url string!';
    }
    return window.atob(output);
  }
});
