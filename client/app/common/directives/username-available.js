angular.module('co.directives.username-available', [
  'co.http'
])
.directive('usernameAvailableValidator', ['httpService', '$q', function(httpService, $q) {
  return {
    require : 'ngModel',
    link : function($scope, element, attrs, ngModel) {
      ngModel.$asyncValidators.usernameAvailable = function(username) {
        return httpService.usernameExists(username || '').then(function(res) {
          if(res.exists) {
            return $q.reject();
          }
          return true;
        });
      };
    }
  };
}]);

