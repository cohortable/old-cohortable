/**
 * ## HTTP Service
 * Basic HTTP wrapper service that all other pieces of the client-side application should go
 * through when making http requests.
 *
 * Request interceptor will prepend the versioned API for our server-side routes
 */
angular.module('co.http', [])
.factory('httpService', function($http) {

  var service = {
    login: login,
    logout: logout,
    register: register,
    usernameExists: usernameExists
  };

  return service;

  // Login to the application
  function login(username, password) {
    return $http.post('/authentication/login', {
      username: username,
      password: password
    })
    .then(function(result) {
      return result.data;
    });
  }

  // Logout
  function logout() {
    return $http.post('/authentication/logout')
    .then(function(result) {
      return result.data;
    });
  }

  // New user registration
  function register(username, password, passwordConfirmation, email) {
    return $http.post('/authentication/register', {
      username: username,
      password: password,
      passwordConfirmation: passwordConfirmation,
      email: email
    })
    .then(function(result) {
      return result.data;
    });
  }

  // New user registration
  function usernameExists(username) {
    return $http.get('/authentication/username-exists/' + username)
    .then(function(result) {
      return result.data;
    });
  }
});
