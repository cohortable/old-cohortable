angular.module('co.interceptors', [])
/**
 * If the JWT token is set, we are going to set the Authorization HEADER for every outgoing request
 * done using $http. As value part of that header we are going to use Bearer <token>.
 *
 * sessionStorage: Although is not supported in all browsers (you can use a polyfill)
 * we are going to use it instead of cookies ($cookies, $cookieStore) and localStorage:
 * The data persisted there lives until the browser tab is closed.
 */
.factory('requestInterceptor', function requestInterceptor($rootScope, $q, $window) {
  var service = {
    request: request
  };
  return service;

  function request(config) {
    // Only prepre the versioned API for non-static resources. Essentially ignore for anything
    // that is html/css
    if(angular.isDefined($rootScope.apiBaseUrl) && config.url.indexOf('.') === -1) {
      config.url = $rootScope.apiBaseUrl + config.url;
    }
    config.headers = config.headers || {};
    if ($window.sessionStorage.token) {
      config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
    }
    return config;
  }
})
// Silly little interceptor to display RTT performance
.factory('timestampMarker', function timestampMarker() {
  var service = {
    request: request,
    response: response
  };
  return service;

  function request(config) {
    config.requestTimestamp = new Date().getTime();
    return config;
  }

  function response(response) {
    response.config.responseTimestamp = new Date().getTime();
    var time = new Date().getTime() - response.config.requestTimestamp;
    console.log('%s: %s took %dms', response.config.method, response.config.url, time);
    return response;
  }
})
/**
 * This is the over-arching error response interceptor. It will get the response and broadcast it out
 * over the root scope so that an error handler can do something with it
 */
.factory('errorInterceptor', function($rootScope, $q) {
  var service = {
    responseError: responseError
  };
  return service;

  function responseError(response) {
    response.data.errorLevel = 'error';
    if(angular.isArray(response.data.errors)) {
      response.data.message = response.data.errors[0].message;
    }
    $rootScope.$broadcast('co:responseError', response.data);
    return $q.reject(response);
  }
})
.config(['$httpProvider', function httpConfig($httpProvider) {
  $httpProvider.interceptors.push('errorInterceptor');
  $httpProvider.interceptors.push('requestInterceptor');
  $httpProvider.interceptors.push('timestampMarker');
}]);
