(function() {
  'use strict';
  angular.module('cohortable', [
    // Services
    'co.directives',
    'co.http',
    'co.interceptors',
    'co.security',

    // Components
    'co.home',
    'co.login',
    'co.header',
    'co.register',

    // Vendor libs
    'ngAnimate',
    'ngMessages',
    'ngFoobar',
    'ui.router'
  ])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider)
  {
    $urlRouterProvider.otherwise('/');

    $stateProvider
    .state('app', {
      abstract: true,
      views: {
        header: {
          templateUrl: '/app/header/header.tpl.html',
          controller: 'HeaderCtrl',
          controllerAs: 'header'
        },
        content: {
          templateUrl: '/views/main.tpl.html',
        },
        footer: {
          templateUrl: '/app/partials/footer.tpl.html'
        }
      },
      controller: 'AppCtrl'
    });

    $locationProvider.html5Mode(true);
  })
  .run( function($rootScope, $location, $window, security, ngFoobar, $http) {
    $rootScope.$on('$stateChangeStart', function(event, next) {
      //redirect only if both isAuthenticated is false and no token is set
      if (angular.isDefined(next) && angular.isDefined(next.access) && next.access.requiredLogin &&
      (!security.isAuthenticated || !$window.sessionStorage.token)) {
        $location.path('/login');
      }
    });

    // Get the base configuration
    $http.get('/api/config').then(function(config) {
      $rootScope.apiBaseUrl = config.data.apiBaseUrl;
    });

    ngFoobar.setAutoClose(true, 3);
    //ngFoobar.setOpacity(0.8);

    // errorLevels are the same as BS3: success, error, warning, info
    $rootScope.$on('co:responseError', function responseError(event, data) {
      ngFoobar.show(data.errorLevel, data.message);
    });
  })
  .controller('AppCtrl', AppCtrl);

  AppCtrl.$inject = [];

  function AppCtrl() {
  }
})();
