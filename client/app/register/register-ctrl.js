(function() {
  'use strict';

  angular.module('co.register.controller', [
    'ui.router',
    'co.security'
  ])
  .config(function config($stateProvider) {
    $stateProvider.state('app.register', {
      url: 'register',
      views: {
        'content@': {
          controller: 'RegisterCtrl',
          controllerAs: 'register',
          templateUrl: '/app/register/register.tpl.html'
        }
      },
      data: { pageTitle: 'Register' }
    });
  })
  .controller('RegisterCtrl', RegisterCtrl);

  RegisterCtrl.$inject = ['security'];

  function RegisterCtrl(security) {
    var vm = this;

    vm.register = function register() {
      security.register(vm.username, vm.password, vm.passwordConfirmation, vm.email);
    };
  }

})();
