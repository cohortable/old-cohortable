(function() {
  'use strict';

  angular.module('co.login.controller', [
    'ui.router',
    'co.security'
  ])
  .config(function config($stateProvider) {
    $stateProvider.state('app.login', {
      url: 'login',
      views: {
        'content@': {
          controller: 'LoginCtrl',
          controllerAs: 'login',
          templateUrl: '/app/login/login.tpl.html'
        }
      },
      data: { pageTitle: 'Login' }
    });
  })
  .controller('LoginCtrl', LoginCtrl);

  LoginCtrl.$inject = ['security'];

  function LoginCtrl(security) {
    var vm = this;
    vm.security = security;
    vm.login = function login() {
      security.login(vm.username, vm.password);
    };
  }

})();
