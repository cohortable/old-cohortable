(function() {
  'use strict';

  angular.module('co.header.controller', [
    'co.security'
  ])
  .controller('HeaderCtrl', HeaderCtrl);

  HeaderCtrl.$inject = ['security'];

  function HeaderCtrl(security) {
    var vm = this;
    vm.security = security;
  }

})();
