# docker-strider

Modification of the Semi-official docker build. It contains supervisord, latest strider, its dependent modules and nodejs/npm.

The configuration file contains sensative information regarding API keys and passwords so make sure
this is kept in a safe place.

We are using two free accounts with the strider instance:

Mailgun - allows us to send out 10k emails per month which we use to send out emails from Strider
MongoLabs - allows us to store 500MB of data in 'the cloud' which we use for strider build plans and build information

## Building

```
make build
```

## Running

There are two different ways to run strider, the first is to run it in test which will not daemonize the process
and it will clean up afterwards

```
make test
```

The other way that will be used to start the instance permanently which will daemonize and not cleanup after running

```
make strider
```

## Security

This image can generate an admin user with a random password. Set environment variable `GENERATE_ADMIN_USER` to use this feature.
