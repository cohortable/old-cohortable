describe('HomeCtrl', function() {
  var ctrl;

  beforeEach(module('co.home'));
  beforeEach(inject(function($controller, security) {
    ctrl = $controller('HomeCtrl');
  }));

  it('should exist', function() {
    expect(angular.isDefined(ctrl)).toBeTruthy();
  });

  it('should handle login attempt', inject(function(security) {
    spyOn(security, 'login');
    ctrl.username = 'username';
    ctrl.password = 'password';
    ctrl.login();
    expect(security.login).toHaveBeenCalledWith('username', 'password');
  }));

  it('should handle register attempt', inject(function(security) {
    spyOn(security, 'register');
    ctrl.username = 'username';
    ctrl.password = 'password';
    ctrl.passwordConfirmation = 'password';
    ctrl.email = 'email';
    ctrl.register();
    expect(security.register).toHaveBeenCalledWith('username', 'password', 'password', 'email');
  }));
});

