describe('app', function() {
  var $rootScope, $location, $state;
  var stateProvider;
  var A, B;

  beforeEach(module('cohortable'));
  beforeEach(module(function($stateProvider) {
    stateProvider = $stateProvider;
    A = { access: { requiredLogin: true } };
    B = { access: { requiredLogin: false } };

    $stateProvider
    .state('A', A)
    .state('B', B);
  }));
  beforeEach(inject(function(_$rootScope_, _$location_, _$state_) {
    $rootScope = _$rootScope_;
    $location = _$location_;
    $state = _$state_;
  }));

  describe('start-up', function() {
    it('should list for response errors', inject(function(ngFoobar) {
      spyOn(ngFoobar, 'show');
      $rootScope.$broadcast('co:responseError', {
        errorLevel: 'error',
        message: ''
      });
      expect(ngFoobar.show).toHaveBeenCalled();
    }));

    it('should reject route changes if user is not authenticated', function() {
      spyOn($location, 'path');
      $state.transitionTo('A', {});
      expect($location.path).toHaveBeenCalledWith('/login');
    });

    it('should reject route changes if no token is present', inject(function(security) {
      spyOn($location, 'path');
      security.isAuthenticated = true;
      $state.transitionTo('A', {});
      expect($location.path).toHaveBeenCalledWith('/login');
    }));

    it('should allow route changes if authenticated with a token', inject(function(security, $window) {
      spyOn($location, 'path');
      security.isAuthenticated = true;
      $window.sessionStorage.token = 'foo';
      $rootScope.$apply(function() {
        $state.transitionTo('A', {});
      });
      expect($state.current).toBe(A);
    }));

    it('should redirect to home page on undefined state', inject(function(security, $window) {
      spyOn($location, 'path');
      $rootScope.$apply(function() {
        $location.path('/foo');
      });
      expect($location.path).toHaveBeenCalledWith('/');
    }));

  });

  describe('AppController', function() {
    var ctrl;

    beforeEach(inject(function($controller, security) {
      ctrl = $controller('AppCtrl');
    }));

    it('should exist', function() {
      expect(angular.isDefined(ctrl)).toBeTruthy();
    });
  });

});
