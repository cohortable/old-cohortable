describe('Interceptors', function() {
  var requestInterceptor;
  var timestampMarker;
  var errorInterceptor;
  var $rootScope;

  beforeEach(module('co.interceptors'));

  beforeEach(inject(function($injector, _$rootScope_) {
    requestInterceptor = $injector.get('requestInterceptor');
    timestampMarker = $injector.get('timestampMarker');
    errorInterceptor = $injector.get('errorInterceptor');
    $rootScope = _$rootScope_;
  }));

  describe('error', function() {
    it('broadcasts on rootScope on error', inject(function($location) {
      spyOn($rootScope, '$broadcast');
      var httpResponse = {
        status: 400,
        data: {
          message: 'BAD'
        }
      };
      errorInterceptor.responseError(httpResponse);
      expect($rootScope.$broadcast).toHaveBeenCalled();
    }));

    it('does not intercept 200 responses', inject(function($location) {
      spyOn($rootScope, '$on');
      var httpResponse = {
        status: 200,
        data: {
          message: 'GOOD'
        }
      };
      errorInterceptor.responseError(httpResponse);
      expect($rootScope.$on).not.toHaveBeenCalled();
    }));
  });

  describe('request', function() {
    it('should append the token if one exists', inject(function($window) {
      var config = { headers: { } };
      $window.sessionStorage.token = 'foo';
      var result = requestInterceptor.request(config);
      expect(result).toEqual({ headers: { Authorization: 'Bearer foo' }});
    }));

    it('should not append the token if one does not exists', inject(function($window) {
      var config = { };
      delete $window.sessionStorage.token;
      var result = requestInterceptor.request(config);
      expect(result).toEqual(config);
    }));
  });

  describe('timestamp', function() {
    it('should mark a timestamp', function() {
      spyOn(console, 'log');
      var config = { };
      config = timestampMarker.request(config);
      expect(angular.isDefined(config.requestTimestamp)).toBeTruthy();
      var result = timestampMarker.response({ config: config });
      expect(console.log).toHaveBeenCalled();
    });

    it('should not append the token if one does not exists', inject(function($window) {
      var config = { headers: { } };
      var result = requestInterceptor.request(config);
      expect(result).toEqual(config);
    }));
  });

});
