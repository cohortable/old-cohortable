describe('RegisterCtrl', function() {
  var ctrl;

  beforeEach(module('co.register'));
  beforeEach(inject(function($controller, security) {
    ctrl = $controller('RegisterCtrl');
  }));

  it('should exist', function() {
    expect(angular.isDefined(ctrl)).toBeTruthy();
  });

  it('should handle register attempt', inject(function(security) {
    spyOn(security, 'register');
    ctrl.username = 'username';
    ctrl.password = 'password';
    ctrl.passwordConfirmation = 'password';
    ctrl.email = 'email';
    ctrl.register();
    expect(security.register).toHaveBeenCalledWith('username', 'password', 'password', 'email');
  }));
});
