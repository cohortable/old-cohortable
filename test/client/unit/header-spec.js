describe('HeaderCtrl', function() {
  var ctrl;

  beforeEach(module('co.header'));
  beforeEach(inject(function($controller) {
    ctrl = $controller('HeaderCtrl');
  }));

  it('should exist', function() {
    expect(angular.isDefined(ctrl)).toBeTruthy();
  });
});
