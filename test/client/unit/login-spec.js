describe('LoginCtrl', function() {
  var ctrl;

  beforeEach(module('co.login'));
  beforeEach(inject(function($controller, security) {
    ctrl = $controller('LoginCtrl');
  }));

  it('should exist', function() {
    expect(angular.isDefined(ctrl)).toBeTruthy();
  });

  it('should handle login attempt', inject(function(security) {
    spyOn(security, 'login');
    ctrl.username = 'username';
    ctrl.password = 'password';
    ctrl.login();
    expect(security.login).toHaveBeenCalledWith('username', 'password');
  }));
});
