describe('security', function() {
  var $rootScope;
  var $httpBackend;
  var httpService;
  var security;
  var token = {
    'token':'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Im5hdGhhc20iLCJhZG1pbiI6dHJ1ZSwiY3JlYXRlZCI6IjIwMTQtMTItMjlUMjI6MDE6NDUuMTIzWiIsImVtYWlsIjoibmF0aGFzbUBnbWFpbC5jb20iLCJpYXQiOjE0MjAxNzM5ODQsImV4cCI6MTQyMDE3NzU4NH0.OF4znvim0xUzEGIgtuw4IgjApic48_FQJO9A3L4Me4Q'
  };
  var loginResponse = {
    username: 'nathasm',
    admin: true,
    created: '2014-12-29T22:01:45.123Z',
    email: 'nathasm@gmail.com',
    iat: 1420173984,
    exp: 1420177584
  };

  beforeEach(module('co.security'));
  beforeEach(inject(function(_$rootScope_, _$httpBackend_, _httpService_, _security_) {
    $rootScope = _$rootScope_;
    $httpBackend = _$httpBackend_;
    httpService = _httpService_;
    security = _security_;
  }));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('register', function() {
    it('should not be able to register while being logged in', function() {
      security.currentUser = 'foo';
      security.isAuthenticated = true;
      security.register('username', 'password');
      expect(security.currentUser).toEqual('foo');
    });

    it('sends a http request to register', inject(function($location) {
      security.isAuthenticated = false;
      $httpBackend.when('POST', '/api/register').respond(200, token);
      $httpBackend.expect('POST', '/api/register');
      security.register('username', 'password', 'password', 'email');
      spyOn($location, 'path');
      $httpBackend.flush();
      expect($location.path).toHaveBeenCalledWith('/');
    }));
  });

  describe('login', function() {
    it('sends a http request to login the specified user', function() {
      $httpBackend.when('POST', '/api/login').respond(200, token);
      $httpBackend.expect('POST', '/api/login');
      security.login('username', 'password');
      $httpBackend.flush();
      expect(security.currentUser).toEqual(loginResponse);
    });
  });

  describe('logout', function() {
    beforeEach(function() {
      $httpBackend.when('POST', '/api/logout').respond(200, {});
    });

    it('sends a http post to clear the current logged in user', function() {
      $httpBackend.expect('POST', '/api/logout');
      security.logout();
      $httpBackend.flush();
    });

    it('redirects to / by default', function() {
      inject(function($location) {
        spyOn($location, 'path');
        security.logout();
        $httpBackend.flush();
        expect($location.path).toHaveBeenCalledWith('/');
      });
    });
  });

  describe('url_base64_decoder', function() {
    it('can decode Base64-encoded input', function() {
      expect(security.url_base64_decode('')).toEqual('');
      expect(security.url_base64_decode('Zg==')).toEqual('f');
      expect(security.url_base64_decode('Zm8=')).toEqual('fo');
      expect(security.url_base64_decode('Zm9v')).toEqual('foo');
      expect(security.url_base64_decode('cXV1eA')).toEqual('quux');
      expect(security.url_base64_decode('cXV1eA==')).toEqual('quux');
      expect(security.url_base64_decode('ISIjJCU=')).toEqual('!"#$%');
      expect(security.url_base64_decode('JicoKSor')).toEqual("&'()*+");
      expect(security.url_base64_decode('LC0uLzAxMg==')).toEqual(',-./012');
      expect(security.url_base64_decode('MzQ1Njc4OTo=')).toEqual('3456789:');
      expect(security.url_base64_decode('Ozw9Pj9AQUJD')).toEqual(';<=>?@ABC');
      expect(security.url_base64_decode('REVGR0hJSktMTQ==')).toEqual('DEFGHIJKLM');
      expect(security.url_base64_decode('Tk9QUVJTVFVWV1g=')).toEqual('NOPQRSTUVWX');
      expect(security.url_base64_decode('WVpbXF1eX2BhYmM=')).toEqual('YZ[\\]^_`abc');
      expect(security.url_base64_decode('ZGVmZ2hpamtsbW5vcA==')).toEqual('defghijklmnop');
      expect(security.url_base64_decode('cXJzdHV2d3h5ent8fX4=')).toEqual('qrstuvwxyz{|}~');
    });

    it('can not decode invalid input', function() {
      try {
        security.url_base64_decode('a');
      } catch(e) {
        expect(e).toEqual('Illegal base64url string!');
      }
    });

  });

});
