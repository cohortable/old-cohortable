describe('usernameAvailableValidator', function() {
  var $httpBackend;
  var $scope;
  var $compile;
  var elem;

  beforeEach(module('co.directives.username-available'));
  beforeEach(inject(function(_$compile_, _$httpBackend_, $rootScope) {
    $httpBackend = _$httpBackend_;
    $scope = $rootScope.$new();
    $compile = _$compile_;
    elem = angular.element('<input name="username" ng-model="val" username-available-validator />');
    $compile(elem)($scope);
  }));

  it('should not allow an undefined value to be sent to the server', function() {
    $httpBackend.expectGET('/api/username-exists/').respond({ exists: true });
    $scope.val = undefined;
    $scope.$digest();
    $httpBackend.flush();
    var ngModelCtrl = elem.controller('ngModel');
    expect(ngModelCtrl.$error.usernameAvailable).toBeTruthy();
  });

  it('should return true if the username already exists', function() {
    $httpBackend.expectGET('/api/username-exists/username').respond({ exists: true });
    $scope.val = 'username';
    $scope.$digest();
    $httpBackend.flush();
    var ngModelCtrl = elem.controller('ngModel');
    expect(ngModelCtrl.$error.usernameAvailable).toBeTruthy();
  });

  it('should return false if the username does not exists', function() {
    $httpBackend.expectGET('/api/username-exists/username').respond({ exists: false });
    $scope.val = 'username';
    $scope.$digest();
    $httpBackend.flush();
    var ngModelCtrl = elem.controller('ngModel');
    expect(ngModelCtrl.$error.usernameAvailable).not.toBeDefined();
  });
});
