module.exports = function (config) {
  config.set({
    basePath: '../..',
    frameworks: ['jasmine'],
    browsers: ['PhantomJS'],
    colors: true,
    reporters: ['dots', 'junit', 'coverage'],
    autoWatch: false,
    singleRun: true,
    exclude: [
      '**/.*.swp'
    ],
    junitReporter: {
      outputFile: './unit.xml',
      suite: 'unit'
    },
    coverageReporter: {
      dir: '.',
      reporters: [
        { type: 'text' },
        { type: 'cobertura', subdir: 'coverage', file: 'cobertura-client.xml' }
      ]
    },

    preprocessors: {
      'client/**/*.html': ['ng-html2js'],
      'client/!(vendor)/**/*.js': 'coverage'
    },

    files: [
      // 3rd party libs
      'client/vendor/jquery/dist/jquery.js',
      'client/vendor/angular/angular.js',
      'client/vendor/angular-animate/angular-animate.js',
      'client/vendor/angular-messages/angular-messages.js',
      'client/vendor/angular-mocks/angular-mocks.js',
      'client/vendor/angular-ui-router/release/angular-ui-router.js',
      'client/vendor/bootstrap/dist/js/bootstrap.js',
      'client/vendor/ng-foobar/ng-foobar.js',
      // App-specific code
      'client/app/**/*.js',
      'client/app/**/*.html',

      // Test-specs
      'test/client/unit/**/*-spec.js'
    ],

    ngHtml2JsPreprocessor: {
      stripPrefix: 'client/'
    }
  });
};
