var supertest = require('supertest');
var app = require('../../server/server');
var routes = require('../../server/routes');

describe('Server', function() {
  it('should handle unknown routes', function(done) {
    supertest(app)
    .get('/bad')
    .expect('Content-Type', /html/)
    .end(function(err, res) {
      res.status.should.equal(200);
    });

    supertest(app)
    .get('/bad')
    .set('Accept', 'text/html')
    .expect('Content-Type', /text/)
    .end(function(err, res) {
      res.status.should.equal(200);
      done();
    });
  });

  it('should have a 404 handler', function(done) {
    supertest(app)
    .post(routes.apiBaseUrl + '/authentication/registe')
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      res.status.should.equal(404);
    });

    supertest(app)
    .post(routes.apiBaseUrl + '/authentication/registe')
    .set('Accept', 'text/html')
    .expect('Content-Type', /text/)
    .end(function(err, res) {
      res.status.should.equal(404);
      done();
    });
  });
});
