var supertest = require('supertest');
var api = require('../../server/api');
var app = require('../../server/server');
var routes = require('../../server/routes');

describe('Companies', function() {
  describe('API', function() {
    it('should return 5 elements when doing a general search', function() {
      supertest(app)
      .get(routes.apiBaseUrl + '/companies')
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        res.body.should.have.a.lengthOf(5);
      });
    });

    it('should return 1 element when doing a specific lookup', function() {
      supertest(app)
      .get(routes.apiBaseUrl + '/companies/1')
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        res.body.should.have.property('company');
      });
    });
  });

  describe('Model', function() {
    it('should return a list of companies', function() {
      api.companies.list().then(function(list) {
        list.should.have.a.length(5);
      });
    });
    it('should return a company', function() {
      api.companies.read().then(function(list) {
        list.should.have.property('company');
      });
    });
  });
});
