debug = require('debug')('cohort:test:authentication');
var f = require('faker');
var assert = require('assert');
var bluebird = require('bluebird');
var supertest = require('supertest');
var app = require('../../server/server');
var routes = require('../../server/routes');
var bookshelf = require('../../server/config/bookshelf');

var api = require('../../server/api');

// Ignore bluebird warning for tests
bluebird.onPossiblyUnhandledRejection();

var getRegistrationOptions = function() {
  return {
    username: f.internet.userName(),
    password: f.internet.password(),
    passwordConfirmation: f.internet.password(),
    email: f.internet.email()
  };
};

var getLoginOptions = function() {
  return {
    username: f.internet.userName(),
    password: f.internet.password(),
  };
};

describe('Auth', function() {
  describe('API', function() {
    before(function (done) {
      bookshelf.knex.migrate.latest().then(function() {
        bookshelf.knex.seed.run().then(function() {
          done();
        });
      });
    });

    after(function(done) {
      bookshelf.knex.migrate.rollback().then(function() {
        done();
      });
    });

    it('should not be able to register with parameters missing', function(done) {
      supertest(app)
      .post(routes.apiBaseUrl + '/authentication/register')
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        res.status.should.equal(400);
        done();
      });
    });

    it('should not be able to register with all the parameters', function(done) {
      supertest(app)
      .post(routes.apiBaseUrl + '/authentication/register')
      .send({username: 'username', password: 'password', passwordConfirmation: '', email: 'user@password.com'})
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        res.status.should.equal(400);
        done();
      });
    });

    it('should be able to register with all the parameters', function(done) {
      supertest(app)
      .post(routes.apiBaseUrl + '/authentication/register')
      .send({username: 'username', password: 'password', passwordConfirmation: 'password', email: 'user@password.com'})
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        res.status.should.equal(200);
        done();
      });
    });

    it('should be able to login', function(done) {
      supertest(app)
      .post(routes.apiBaseUrl + '/authentication/login')
      .send({username: 'username', password: 'password'})
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        res.status.should.equal(200);
        done();
      });
    });

    it('should complain if unable to login', function(done) {
      supertest(app)
      .post(routes.apiBaseUrl + '/authentication/login')
      .send({})
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        res.status.should.equal(401);
        res.body.should.eql({ errors: [
          { type: 'AuthenticationError', message: 'Invalid username or password' }
        ]});
        done();
      });
    });

    it('should be able to logout', function(done) {
      supertest(app)
      .post(routes.apiBaseUrl + '/authentication/logout')
      .send({user: 'username'})
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        res.status.should.equal(200);
        done();
      });
    });

    it('should be able to check for an existing username', function(done) {
      supertest(app)
      .get(routes.apiBaseUrl + '/authentication/username-exists/username')
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        res.status.should.equal(200);
        res.body.should.eql({ exists: true });
        done();
      });
    });

    it('should be able to check for a non-existant username', function(done) {
      supertest(app)
      .get(routes.apiBaseUrl + '/authentication/username-exists/foobar')
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        res.status.should.equal(200);
        res.body.should.eql({ exists: false });
        done();
      });
    });
  });

  describe('Registration', function() {
    before(function (done) {
      bookshelf.knex.migrate.latest().then(function() {
        bookshelf.knex.seed.run().then(function() {
          done();
        });
      });
    });

    after(function(done) {
      bookshelf.knex.migrate.rollback().then(function() {
        done();
      });
    });

    it('should complain on empty username', function() {
      var options = getRegistrationOptions();
      options.username = options.password = '';
      return api.authentication.register(options)
      .then(assert.fail)
      .catch(function(err) {
        err.code.should.equal(400);
        err.type.should.equal('BadRequestError');
        err.message.should.equal('Username and password must be provided');
      });
    });
    it('should complain on empty password', function() {
      var options = getRegistrationOptions();
      options.password = '';
      return api.authentication.register(options)
      .then(assert.fail)
      .catch(function(err) {
        err.code.should.equal(400);
        err.type.should.equal('BadRequestError');
        err.message.should.equal('Username and password must be provided');
      });
    });
    it('should complain on empty confirmation password', function() {
      var options = getRegistrationOptions();
      options.passwordConfirmation = '';
      return api.authentication.register(options)
      .then(assert.fail)
      .catch(function(err) {
        err.code.should.equal(400);
        err.type.should.equal('BadRequestError');
        err.message.should.equal('Passwords do not match');
      });
    });
    it('should complain on empty email', function() {
      var options = getRegistrationOptions();
      options.password = options.passwordConfirmation;
      options.email = '';
      return api.authentication.register(options)
      .then(assert.fail)
      .catch(function(err) {
        err.code.should.equal(400);
        err.type.should.equal('BadRequestError');
        err.message.should.equal('Email must be provided');
      });
    });

    it('should complain if passwords do not match', function() {
      return api.authentication.register(getRegistrationOptions())
      .then(assert.fail)
      .catch(function(err) {
        err.code.should.equal(400);
        err.type.should.equal('BadRequestError');
        err.message.should.equal('Passwords do not match');
      });
    });

    it('should be able to register a new user', function() {
      var options = getRegistrationOptions();
      options.password = options.passwordConfirmation;
      return api.authentication.register(options);
    });

    it('should complain if username already exists', function() {
      var options = getRegistrationOptions();
      options.password = options.passwordConfirmation;
      return api.authentication.register(options)
      .then(function() {
        return api.authentication.register(options);
      })
      .then(assert.fail)
      .catch(function(err) {
        err.code.should.equal(400);
        err.message.should.equal('Username already exists');
      });
    });

    it('should complain if email is in a bad format', function() {
      var options = getRegistrationOptions();
      options.password = options.passwordConfirmation;
      options.email = 'A b c';
      return api.authentication.register(options)
      .then(assert.fail)
      .catch(function(err) {
        err.code.should.equal(422);
        err.message.should.equal('The email must be a valid email address');
      });
    });
  });

  describe('Login', function() {
    before(function (done) {
      bookshelf.knex.migrate.latest().then(function() {
        return bookshelf.knex.seed.run();
      }).then(function() {
        var options = getRegistrationOptions();
        options.username = 'username';
        options.password = options.passwordConfirmation = 'password';
        return api.authentication.register(options);
      }).then(function() {
        done();
      });
    });

    after(function(done) {
      bookshelf.knex.migrate.rollback().then(function() {
        done();
      });
    });

    it('should not be able to login with empty user', function() {
      var options = getLoginOptions();
      options.username = options.password = '';
      return api.authentication.login(options)
      .then(assert.fail)
      .catch(function(err) {
        err.code.should.equal(401);
        err.message.should.equal('Invalid username or password');
      });
    });

    it('should not be able to login with empty password', function() {
      var options = getLoginOptions();
      options.password = '';
      return api.authentication.login(options)
      .then(assert.fail)
      .catch(function(err) {
        err.code.should.equal(401);
        err.message.should.equal('Invalid username or password');
      });
    });

    it('should not be able to login with unknown user', function() {
      var options = getLoginOptions();
      return api.authentication.login(options)
      .then(assert.fail)
      .catch(function(err) {
        err.code.should.equal(401);
        err.message.should.equal('Invalid username or password');
      });
    });

    it('should not be able to login with bad password', function() {
      var options = getLoginOptions();
      options.username = 'username';
      return api.authentication.login(options)
      .then(assert.fail)
      .catch(function(err) {
        err.code.should.equal(401);
        err.message.should.equal('Invalid username or password');
      });
    });

    it('should be able to login with valid credentials', function() {
      var options = {
        username: 'username',
        password: 'password'
      };
      return api.authentication.login(options)
      .then(function(token) {
        token.should.have.property('token');
        var req = { headers: { authorization: 'Bearer ' + token.token } };
        var res = {};
        require('express-jwt')({secret: require('../../server/config').JWTsecret })(req, res, function() {
          var user = req.user;
          user.username.should.equal('username');
        });
      })
      .catch(assert.fail);
    });
  });

  describe('Logout', function() {
    it('should remove user object when logging out', function() {
      var req = { user: 'foo' };
      return api.authentication.logout(req)
      .then(function() {
        req.should.not.have.property('user');
      });
    });
  });

  describe('Username Exists', function() {
    before(function (done) {
      bookshelf.knex.migrate.latest().then(function() {
        return bookshelf.knex.seed.run();
      }).then(function() {
        var options = getRegistrationOptions();
        options.username = 'username';
        options.password = options.passwordConfirmation;
        return api.authentication.register(options);
      }).then(function() {
        done();
      });
    });

    after(function(done) {
      bookshelf.knex.migrate.rollback().then(function() {
        done();
      });
    });

    it('should return true if a username already exists', function() {
      return api.authentication.usernameExists({ username: 'username' })
      .then(function(result) {
        result.should.eql({ exists: true });
      });
    });

    it('should return false if a username does not exist', function() {
      return api.authentication.usernameExists({ username: 'foobar' })
      .then(function(result) {
        result.should.eql({ exists: false });
      });
    });
  });
});
