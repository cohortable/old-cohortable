var debug = require('debug')('cohort:api:authentication');
var jwt = require('jsonwebtoken');
var config = require('../config');
var errors = require('../errors');
var bluebird = require('bluebird');
var dataProvider = require('../models');
var _ = require('lodash');
var auth;

/**
 * ## Authentication API Methods
 */

authentication = {
  /**
   * ## Register a new user
   * @param {Object} object containing registration information
   * @returns {Promise(userData)} new user credentials
   */
  register : function register(options) {
    var attrs = ['username', 'password', 'email', 'passwordConfirmation'];
    var data = _.pick(options, attrs);

    //Angular form validation also ensures required fields are filled
    //Check to ensure passwordConfirmation matches password
    if (!data.username || data.username === '' || !data.password || data.password === '') {
      debug('Username/Password were not provided');
      return bluebird.reject(new errors.BadRequestError('Username and password must be provided'));
    } else if(data.password != data.passwordConfirmation) {
      debug('passwords do not match');
      return bluebird.reject(new errors.BadRequestError('Passwords do not match'));
    } else if(data.email === '') {
      debug('email is empty');
      return bluebird.reject(new errors.BadRequestError('Email must be provided'));
    }

    delete data.passwordConfirmation;
    return dataProvider.User.add(data);
  },

  /**
   * ## Login an existing user
   * @param {Object} object containing login information
   * @returns {Promise(userData)} user credentials
   */
  login : function login(options) {
    var attrs = ['username', 'password'];
    var data = _.pick(options, attrs);
    var user;

    debug(data);
    return dataProvider.User.getByUsername(data.username).then(function(result) {
      if (!result) {
        debug('User undefined, throwing error');
        return bluebird.reject(new errors.AuthenticationError('Invalid username or password'));
      }
      user = result.toJSON();

      debug(data.password, user.password);
      return dataProvider.User.verifyPassword(data.password, user.password);
    })
    .then(function(isMatch) {
      if (!isMatch) {
        debug('Attempt failed to login with %s, password does not match', data.username);
        return bluebird.reject(new errors.AuthenticationError('Invalid username or password'));
      }
      var userProfile = {
        username: user.username,
        created: user.created,
        email: user.email
      };

      /**
      Build the JWT - using jsonwebtoken.js method sign(payload, secretOrPrivateKey, options)
      return type is a string
      put users profile inside the JWT (payload)
      Set token to expire in 60 min (option)
      */
      var token = jwt.sign(userProfile, config.JWTsecret, { expiresInMinutes: 60*1 });

      // Send the token as JSON to user
      return {
        token: token
      };
    }).catch(function(err) {
      return bluebird.reject(err);
    });
  },

  /**
   * ## Logs out a user
   * @param {req} HTTP request object
   * @param {res} HTTP response object
   * @returns {response} returns an empty response
   */
  logout: function logout(req) {
    delete req.user;
    return bluebird.resolve();
  },

  /**
   * ## Checks to see if a username already exists in the DB
   * @param {options} Infomration required to look up an existing user
   * @returns {bool} returns true/false based upon user existance
   */
  usernameExists: function usernameExists(options) {
    return dataProvider.User.getByUsername(options.username).then(function(result) {
      return { exists: !!result };
    });
  }
};

module.exports = authentication;
