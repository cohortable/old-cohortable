var debug = require('debug')('cohort:api');
var _ = require('lodash');
var authentication = require('./authentication');
var companies = require('./companies');
var http;

/**
 * ### Format HTTP Errors
 * Converts the error response from the API into a format which can be returned over HTTP
 *
 * @private
 * @param {Array} error
 * @return {{errors: Array, statusCode: number}}
 */
formatHttpErrors = function (error) {
  var statusCode = 500,
  errors = [];

  if (!_.isArray(error)) {
    error = [].concat(error);
  }

  _.each(error, function (errorItem) {
    var errorContent = {};

    // TODO: add logic to set the correct status code
    statusCode = errorItem.code || 500;

    errorContent.message = _.isString(errorItem) ? errorItem :
    (_.isObject(errorItem) ? errorItem.message : 'Unknown API Error');
    errorContent.type = errorItem.type || 'InternalServerError';
    errors.push(errorContent);
  });

  return {errors: errors, statusCode: statusCode};
};

/**
 * ### HTTP
 *
 * Decorator for API functions which are called via an HTTP request. Takes the API method and wraps it so that it gets
 * data from the request and returns a sensible JSON response.
 *
 * @public
 * @param {Function} apiMethod API method to call
 * @return {Function} middleware format function to be called by the route when a matching request is made
 */
http = function (apiMethod) {
  return function (req, res) {
    // We define 2 properties for using as arguments in API calls:
    var object = req.body,
    options = _.extend({}, req.files, req.query, req.params, {
      context: {
        user: (req.user && req.user.id) ? req.user.id : null
      }
    });

    // If this is a GET, or a DELETE, req.body should be null, so we only have options (route and query params)
    // If this is a PUT, POST, or PATCH, req.body is an object
    if (_.isEmpty(object)) {
      object = options;
      options = {};
    }

    return apiMethod(object, options).then(function (response) {
      // Send a properly formatting HTTP response containing the data with correct headers
      res.json(response || {});
    }).catch(function onError(error) {
      debug('ERROR: %s', error);
      var httpErrors = formatHttpErrors(error);
      // Send a properly formatted HTTP response containing the errors
      res.status(httpErrors.statusCode).json({errors: httpErrors.errors});
    });
  };
};

module.exports = {
  authentication: authentication,
  companies: companies,
  http: http
};
