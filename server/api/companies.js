var f = require('faker');
var bluebird = require('bluebird');
var _ = require('lodash');

var generateCompany = function() {
  return {
    company: {
      name: f.company.companyName(),
      logo: f.image.abstract(),
      website: f.internet.domainName(),
      contact: f.name.findName(),
      location: {
        address: f.address.streetAddress(),
        city: f.address.city(),
        state: f.address.stateAbbr(),
        zip: f.address.zipCode(),
      },
      description: f.company.bs()
    },
    industry: f.company.catchPhraseNoun(),
    demographics: {
      sex: f.helpers.randomNumber() === 0 ? 'Male' : 'Female',
      age: f.random.number()
    },
    email_list_size: f.finance.mask(),
    social: {
      facebook: f.internet.domainName(),
      linkedin: f.internet.domainName(),
      tumblr: f.internet.domainName(),
      google: f.internet.domainName()
    }
  };
};
module.exports = {
  list: function() {
    return bluebird.resolve(_.times(5, function() { return generateCompany(); } ));
  },

  read: function(req, res) {
    return bluebird.resolve(generateCompany());
  }
};
