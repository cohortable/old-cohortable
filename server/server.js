var express = require('express');
var logger = require('morgan');
var http = require('http');
var debug = require('debug')('cohort:server');
var util = require('util');
var routes = require('./routes');
var app = express();

var env = process.env.NODE_ENV || 'development';
debug('Running in %s mode', env);

app.set('port', process.env.PORT || 3000);

// Based upon the environemnt we are running in, do differnt things
switch(env) {
  case 'development':
  case 'dev':
    debug('ENV: %s', env);
    app.use(logger('dev', { immediate: true }));
    break;
  case 'test':
    break;
  case 'production':
  case 'prod':
    app.use(logger());
    break;
  default:
}

/* Middleware */
app.use(require('body-parser').json());

// When we receive our first request, we will log the parameters that were passed in
app.use(function logParams(req, res, next) {
  if(req.body && req.body.length) { debug('BODY: %s', util.inspect(req.body, false, null)); }
  if(req.params && req.params.length) { debug('PARAMS: %s', util.inspect(req.params, false, null)); }
  if(req.query && req.query.length) { debug('QUERY: %s', util.inspect(req.query, false, null)); }
  next();
});

// serve the static files from the client folder. We must explictly list these
// directories out so that Angular's html5mode works on the client
app.use('/app', express.static(__dirname + '/../client/app'));
app.use('/styles', express.static(__dirname + '/../client/styles'));
app.use('/vendor', express.static(__dirname + '/../client/vendor'));
app.use('/views', express.static(__dirname + '/../client/views'));
app.use('/images', express.static(__dirname + '/../client/images'));

// API endpoint routes. These should all be prefixed with /api. The relative
// routes in each of the require paths will then have the /api/XXX prepended to
// their routes
app.use(routes.apiBaseUrl, routes.api());
app.use('/api/config', function(req, res, next) {
  res.json({
    apiBaseUrl: routes.apiBaseUrl
  });
});

// If a request for only '/' comes in, then send back index.html, otherwise let the catch-all route
// handle it
app.use('^/favicon.ico$', function(req, res, next) {
  res.sendfile('favicon.ico', { root: __dirname + '/../client/images' });
});
app.use('^/$', function(req, res, next) {
  res.sendfile('index.html', { root: __dirname + '/../client' });
});
//app.use('[^\.]+$', function(req, res, next) {
  //res.sendfile('index.html', { root: __dirname + '/../client' });
//});

// Unhandle API. How do we handle when someone makes a request of an unknown route
app.use(function handleNotFound(req, res) {
  res.status(404);

  if (req.accepts('json')) {
    res.send({ message: 'Not found: ' + req.url });
    return;
  }

  res.type('txt').send('Not found: ' + req.url);
});

// Error handler, when a middleware calls next() with a parameter, this middleware gets called
app.use(function logErrors(err, req, res, next) {
  debug('ERROR HANDLER: %s', err.stack || err);
  next(err);
});

// Response error handler
app.use(function (err, req, res, next) {
  res.status(err.code || 500);

  // respond with json
  if (req.accepts('json')) {
    res.json({ message: err.message || 'Server Error' });
    return;
  }

  // default to plain-text. send()
  res.type('txt').send(err.message || 'Server Error');
});

// This is useful to make sure we only actually start the server if we're explicitly running
// this module versus having it be require'd from something like a unit test
if(!module.parent) {
  // Start HTTP Server Listening on port if we aren't included by another file
  app.listen(app.get('port'));
  debug('Express server listening on port %d', app.get('port'));
}

process.on('SIGINT', function() {
  debug('\ngracefully shutting down from SIGINT');
  process.exit();
});

process.on('SIGTERM', function() {
  debug('\ngracefully shutting down from SIGTERM');
  process.exit();
});

module.exports = app;
