var _ = require('lodash');
var bluebird = require('bluebird');
var BadRequestError = require('./bad-request-error');
var ValidationError = require('./validation-error');
var AuthenticationError = require('./authentication-error');
var errors;

errors = {
  rejectError: function(err) {
    return bluebird.reject(err);
  },
};

module.exports = errors;
module.exports.BadRequestError = BadRequestError;
module.exports.ValidationError = ValidationError;
module.exports.AuthenticationError = AuthenticationError;
