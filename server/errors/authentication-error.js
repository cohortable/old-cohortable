// # Authentication Error
// Custom error class with status code and type prefilled. Used if the user has trouble logging in

function AuthenticationError(message) {
    this.message = message;
    this.stack = new Error().stack;
    this.code = 401;
    this.type = this.name;
}

AuthenticationError.prototype = Object.create(Error.prototype);
AuthenticationError.prototype.name = 'AuthenticationError';

module.exports = AuthenticationError;
