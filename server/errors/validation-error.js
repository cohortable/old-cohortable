// # Validation Error
// Custom error class with status code and type prefilled and should only be used by errors that
// are generated from checkit errors when trying to save data to the DB.

function ValidationError(error) {
    // Grab all the keys from the object, each key is a field that failed valiation
    // We only care to send back one error at a time
    this.message = error.toString(true).split(':')[1].trim();
    this.stack = new Error().stack;
    this.code = 422;
    this.type = this.name;
}

ValidationError.prototype = Object.create(Error.prototype);
ValidationError.prototype.name = 'ValidationError';

module.exports = ValidationError;
