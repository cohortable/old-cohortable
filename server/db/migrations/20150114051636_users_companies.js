
exports.up = function(knex, Promise) {
  return knex.schema.createTable('users', function(t) {

    t.increments('id').primary();
    t.string('username').unique().notNull();
    t.string('email').unique().notNull();
    t.string('password').notNull();

    t.dateTime('created_at').notNull().defaultTo(knex.raw('now()'));
    t.dateTime('updated_at').nullable().defaultTo(knex.raw('now()'));

  }).createTable('companies', function(t) {

    t.increments('id').primary();
    t.string('name').unique().notNull();
    t.string('logo').notNull();
    t.string('website').notNull();
    t.string('contact').notNull();
    t.string('address').notNull();
    t.string('city').notNull();
    t.string('state').notNull();
    t.string('zip').notNull();
    t.string('description').notNull();
    t.string('industry').notNull();

    t.integer('user_id').unsigned().references('users.id');
    t.integer('demographics_id').unsigned().references('demographics.id').notNull();
    t.integer('ages_id').unsigned().references('ages.id').notNull();
    t.integer('email_list_sizes_id').unsigned().references('email_list_sizes.id').notNull();

    t.dateTime('created_at').notNull().defaultTo(knex.raw('now()'));
    t.dateTime('updated_at').nullable().defaultTo(knex.raw('now()'));

  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('companies').dropTable('users');
};
