module.exports = {

  development: {
    client: 'mysql',
    connection: {
      host: 'localhost',
      user: 'cohortable',
      password: 'CohortableMysql',
      database: 'cohortable_development',
      charset: 'utf8'
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: '../db/migrations'
    },
    seeds: {
      directory: '../db/seeds'
    }
  },

  test: {
    client: 'mysql',
    connection: {
      host: 'localhost',
      user: 'cohortable',
      password: 'CohortableMysql',
      database: 'cohortable_test',
      charset: 'utf8'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: '../db/migrations'
    },
    seeds: {
      directory: '../db/seeds'
    }
  },

  production: {
    client: 'mysql',
    connection: {
      host: 'localhost',
      user: 'cohortable',
      password: 'CohortableMysql',
      database: 'cohortable_production',
      charset: 'utf8'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: '../db/migrations'
    },
    seeds: {
      directory: '../db/seeds'
    }
  },

};
