var api = require('./api');

module.exports = {
  apiBaseUrl: '/cohortable/api/v0.1',
  api: api
};
