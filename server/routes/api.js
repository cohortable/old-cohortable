var debug = require('debug')('cohort:routes:api');
var express = require('express');
var api = require('../api');
var apiRoutes;

apiRoutes = function(middleware) {
  var router = express.Router();

  // Alias delete with del
  router.del = router.delete;

  // ## Authentication
  router.post('/authentication/register', api.http(api.authentication.register));
  router.post('/authentication/login', api.http(api.authentication.login));
  router.post('/authentication/logout', api.http(api.authentication.logout));
  router.get('/authentication/username-exists/:username', api.http(api.authentication.usernameExists));

  // ## Search
  router.get('/companies', api.http(api.companies.list));
  router.get('/companies/:id', api.http(api.companies.read));
  return router;
};

module.exports = apiRoutes;
