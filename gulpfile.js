var gulp = require('gulp');
var fs = require('fs');
var tasks = require('gulp-load-plugins')();
var mocha = require('gulp-spawn-mocha');
var sequence = require('run-sequence');
var path = require('path');
var karma = require('karma').server;
var browserSync = require('browser-sync');
var reload = browserSync.reload;

// Include Our Plugins
var protractor  = tasks.protractor.protractor;
var webdriver   = tasks.protractor.webdriver;

// Compiles the sass files to css files
gulp.task('sass', function() {
  console.log('SASS');
  gulp.src('./client/styles/cohortable.scss')
  .pipe(tasks.plumber())
  .pipe(tasks.sass({
    includePaths: ['./client/styles'],
    errLogToConsole: true
  }))
  .pipe(tasks.sourcemaps.init())
  .pipe(tasks.autoprefixer())
  .pipe(tasks.sourcemaps.write())
  .pipe(gulp.dest('client/styles'))
  .pipe(reload({ stream: true }));
});

gulp.task('bootlint', function() {
  return gulp.src(['./client/**/*.html'])
  .pipe(tasks.bootlint({
    disabledIds: [
      'E001', // Document is missing a DOCTYPE declaration
      'W001', // `<head>` is missing UTF-8 charset `<meta>` tag
      'W002', // `<head>` is missing X-UA-Compatible `<meta>` tag that disables old IE compatibility modes
      'W003', // `<head>` is missing viewport `<meta>` tag that enables responsiveness
      'W005', //  Unable to locate jQuery, which is required for Bootstrap's JavaScript plugins to work
    ]
  }));
});

gulp.task('watch', ['nodemon', 'sass', 'browser-sync'], function() {
  gulp.watch(['./client/styles/*.scss'], ['sass']);
});

gulp.task('nodemon', function() {
  fs.writeFileSync('.nodemon', 'started');
  tasks.nodemon({
    script: 'server/server.js',
    ext: 'js',
    env: { 'NODE_ENV': 'development', 'DEBUG': 'cohort:*' },
    ignore: ['node_modules/**', 'client/**', 'test/**'],
  })
  .on('log', function (event) {
    console.log(event.colour);
  })
  .on('change', function() {
  })
  .on('restart', function () {});
});

/**
 * Enables live reload using browser-sync
 */
gulp.task('browser-sync', function() {
  browserSync.init([
    './client/**/*.js',
    './client/**/*.html',
    ], {
      logConnections: true,
      logLevel: 'debug',
      port: 3100,
      proxy: 'http://localhost:3000/#/',
    });
});


gulp.task('mocha', function() {
  return gulp.src(['test/server/**/*.js'])
  .pipe(mocha({
    require: 'should',
    reporter: 'mocha-jenkins-reporter',
    env: {
      'NODE_ENV': 'test',
      'JUNIT_REPORT_PATH': './report.xml'
    },
    istanbul: {
      report: 'cobertura'
    }
  }))
  .on('error', tasks.util.log);
});

gulp.task('karma', function() {
  return karma.start({
    configFile: path.resolve('test/client/karma.conf.js'),
    singleRun: true,
    autoWatch: false
  });
});

gulp.task('protractor', function() {
  gulp.src('[test/client/e2e/*.js]')
  .pipe(protractor({
    configFile: 'test/client/protractor.conf.js'
  }));
});

gulp.task('refreshdb', tasks.shell.task([
  'knex --knexfile server/config/knexfile.js migrate:rollback',
  'knex --knexfile server/config/knexfile.js migrate:latest',
  'knex --knexfile server/config/knexfile.js seed:run'
]));

gulp.task('default', ['watch']);

gulp.task('test', function() {
  sequence('mocha', 'karma');
});
